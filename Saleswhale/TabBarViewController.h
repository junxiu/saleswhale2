//
//  TabBarViewController.h
//  Saleswhale
//
//  Created by Jun Xiu Chan on 1/14/15.
//  Copyright (c) 2015 Jun Xiu Chan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabBarViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *notificationBadgeContentView;
- (IBAction)didSelectBarButtonItem:(UIButton *)sender;

@property (strong, nonatomic) IBOutletCollection(id) NSArray *tabBarButtonItems;





@end
