//
//  PostCollectionViewCell.h
//  Saleswhale
//
//  Created by Jun Xiu Chan on 1/6/15.
//  Copyright (c) 2015 Jun Xiu Chan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SWRoundImageView.h"

@interface PostCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet SWRoundImageView *authorImageView;
@property (weak, nonatomic) IBOutlet UILabel *actionLabel;
@property (weak, nonatomic) IBOutlet UIView *descriptionView;
@property (weak, nonatomic) IBOutlet UIView *statsView;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

- (void)setAuthorName:(NSString *)nameStr
               action:(NSString *)actionStr
                 verb:(NSString *)verbStr
            timeStamp:(NSString *)timeStr;

@end
