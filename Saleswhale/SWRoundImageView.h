//
//  SWRoundImageView.h
//  Saleswhale
//
//  Created by Jun Xiu Chan on 1/9/15.
//  Copyright (c) 2015 Jun Xiu Chan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWRoundImageView : UIImageView

- (void)setCornerRadius:(CGFloat)cornerRadius;

@end
