//
//  NSString+StringSize.h
//  Saleswhale
//
//  Created by Jun Xiu Chan on 1/10/15.
//  Copyright (c) 2015 Jun Xiu Chan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (StringSize)

- (CGSize)getContentSizeWithFont:(UIFont *)font andMaxSize:(CGSize)maxSize;

@end
