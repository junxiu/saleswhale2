//
//  NSString+StringSize.m
//  Saleswhale
//
//  Created by Jun Xiu Chan on 1/10/15.
//  Copyright (c) 2015 Jun Xiu Chan. All rights reserved.
//

#import "NSString+StringSize.h"

@implementation NSString (StringSize)

- (CGSize)getContentSizeWithFont:(UIFont *)font andMaxSize:(CGSize)maxSize {
    if (self) {
        CGRect frame = [self boundingRectWithSize:CGSizeMake(maxSize.width, maxSize.height)
                                          options:NSStringDrawingUsesLineFragmentOrigin
                                       attributes:@{NSFontAttributeName:font}
                                          context:nil];
        return CGSizeMake(frame.size.width, frame.size.height+1);
    }
    return CGSizeZero;
}

@end
