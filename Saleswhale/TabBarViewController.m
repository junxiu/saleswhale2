//
//  TabBarViewController.m
//  Saleswhale
//
//  Created by Jun Xiu Chan on 1/14/15.
//  Copyright (c) 2015 Jun Xiu Chan. All rights reserved.
//

#import "TabBarViewController.h"

@interface TabBarViewController ()

@end

@implementation TabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupTabBar];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupTabBar {
    [self.notificationBadgeContentView setCornerRadius:self.notificationBadgeContentView.frame.size.height/2];
    
    for (id tabBarButton in self.tabBarButtonItems) {
        [tabBarButton setFrameWidth:self.view.frame.size.width/4];
        [tabBarButton setFrameOriginX:[self.tabBarButtonItems indexOfObject:tabBarButton] * self.view.frame.size.width/4];
    }
}

- (IBAction)didSelectBarButtonItem:(UIButton *)sender {
    for (id tabBarButton in self.tabBarButtonItems) {
        [tabBarButton setAlpha:0.2];
    }
    
    [sender setAlpha:1];
    [[self.tabBarButtonItems objectAtIndex:1] setAlpha:1];
}

@end
