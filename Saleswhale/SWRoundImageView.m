//
//  SWRoundImageView.m
//  Saleswhale
//
//  Created by Jun Xiu Chan on 1/9/15.
//  Copyright (c) 2015 Jun Xiu Chan. All rights reserved.
//

#import "SWRoundImageView.h"

@implementation SWRoundImageView

- (void)awakeFromNib {
    [self setCornerRadius:CGRectGetHeight(self.bounds)/2];
}

- (void)setCornerRadius:(CGFloat)cornerRadius {
    self.layer.cornerRadius = cornerRadius;
}

@end
